#!/bin/bash

DOTFILES_DIRECTORY="${HOME}/dotfiles"

for f in .??*
do
	[ "$f" = ".git" ] && continue
	echo ${f}
	ln -snvf $DOTFILES_DIRECTORY/$f $HOME/$f

done
