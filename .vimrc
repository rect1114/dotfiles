"---------------------------
"this is abc10946's .vimrc
"---------------------------

"mkdir -p ~/.vim/bundle
"git clone https://github.com/Shougo/neobundle.vim ~/.vim/bundle/neobundle.vim

"---------------
"setting neobundle
"---------------
if has('vim_starting')
   set nocompatible               " Be iMproved

   " Required:
   set runtimepath+=~/.vim/bundle/neobundle.vim/
endif

" Required:
call neobundle#begin(expand('~/.vim/bundle/'))

" Let NeoBundle manage NeoBundle
" Required:
" My Bundles here:
":NeoBundleInstall:install plugin
":NeoBundleInstall!:update plugin
"NeoBundleClean:remove plugin

NeoBundleFetch 'Shougo/neobundle.vim'
"useful vim plugin
NeoBundle 'Shougo/unite.vim'
"decolate status bar
NeoBundle 'itchyny/lightline.vim'
"quickrun
NeoBundle 'thinca/vim-quickrun'
"display haskell syntax
NeoBundle 'dag/vim2hs'
"display nim syntax
NeoBundle 'zah/nim.vim'
"display outline view
NeoBundle 'h1mesuke/unite-outline'

" Refer to |:NeoBundle-examples|.

" Note: You don't set neobundle setting in .gvimrc!

call neobundle#end()

" Required:
filetype plugin indent on

" If there are uninstalled bundles found on startup,
" this will conveniently prompt you to install them.
NeoBundleCheck

 "lightline.vimの設定
set laststatus=2
if !has('gui_running')
	set t_Co=256
endif


"--------------
"display setting
"--------------
set expandtab
set tabstop=4
set shiftwidth=4
set number "display number of line
set hlsearch "highlight search result
set title "display title
syntax on
set paste "paste text keeping indent. turn off(:set nopaste)
set shortmess+=I "remove startup message
set autoindent
"note
"if you want you paste text,you should turn off autoindent(use :set noautoindent)
set mouse-=a "disable mouse control


"--------------
"search setting
"--------------
set ignorecase "without distinguish between large alphabet and small alphabet
set smartcase "if large alphabet in search text,distinguish between large alphabet and small alphabet
set wrapscan "return to head of file when reached finish on searching
"note
"if you want to move cursor each result texts,type n to next text.
":noh
"turn off result text highlight.


"----------------
"keybinding
"----------------
nnoremap sj <C-w>j
nnoremap sk <C-w>k
nnoremap sl <C-w>l
nnoremap sh <C-w>h
noremap <silent><C-e> :tabe<CR>
noremap <silent><C-w> :tabn<CR>
inoremap { {}
inoremap ( ()


"-----------------------
"adapted to version 8.0
"-----------------------
set backspace=indent,eol,start
