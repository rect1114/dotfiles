import XMonad
import XMonad.Config.Desktop
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.DynamicLog
import XMonad.Util.EZConfig
import XMonad.Actions.GridSelect

main = do
	xmonad =<< xmobar myConfig

myConfig = desktopConfig {
	 terminal = "gnome-terminal"
	,modMask = mod4Mask
	,borderWidth = 1
	,startupHook = myStartupHook
} `additionalKeysP` myAdditionalKeyBinding

myAdditionalKeyBinding = [
        ("M-S-g", spawn "google-chrome-stable") -- Mod+Shift+g : start google-chrome
       ,("M-S-s", spawn "gnome-screenshot")     -- Mod+Shift+s : take a screenshot
       ,("M-S-n", spawn "nautilus")             -- Mod+Shift+n : open nautilus
       ,("<XF86AudioLowerVolume>", spawn "python $HOME/.xmonad/extras/alsa-control.py dec 4;gst-play-1.0 /usr/share/sounds/freedesktop/stereo/message.oga")
       ,("<XF86AudioRaiseVolume>", spawn "python $HOME/.xmonad/extras/alsa-control.py inc 4;gst-play-1.0 /usr/share/sounds/freedesktop/stereo/message.oga")
	   ,("<XF86AudioMute>",spawn "python $HOME/.xmonad/extras/alsa-control.py toggle 0;")
	   ,("<XF86MonBrightnessUp>",spawn "xbacklight -inc 5")
	   ,("<XF86MonBrightnessDown>",spawn "xbacklight -dec 5")
	   ,("M-g",spawnSelected defaultGSConfig applications)
	   ,("M-S-l", spawn "light-locker-command -l")
	]

applications = ["baobab","nautilus","google-chrome-stable","code","gedit","urxvt","reboot","poweroff","gnome-terminal","light-locker-command -l"]
myStartupHook = do
	spawn "feh --bg-center /usr/share/backgrounds/gnome/Road.jpg"
