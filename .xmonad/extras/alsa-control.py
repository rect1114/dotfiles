import alsaaudio
import sys


try:
    mixer = alsaaudio.Mixer("Master")
except alsaaudio.ALSAAudioError:
    sys.exit(1)

currentVolume = mixer.getvolume()[0]
channel = alsaaudio.MIXER_CHANNEL_ALL

if len(sys.argv) > 2:
    mode = sys.argv[1] #"inc" or "dec" or "toggle"
    deltaVolume = int(sys.argv[2]) #volume 0 - 100
else:
    sys.exit(1)


if mode == "inc":
    volume = currentVolume + deltaVolume
    if volume < 100:
        mixer.setvolume(volume,channel)
    else:
        volume = 100
        mixer.setvolume(100,channel)

elif mode == "dec":
    volume = currentVolume - deltaVolume
    if volume > 0:
        mixer.setvolume(volume,channel)
    else:
        volume = 0
        mixer.setvolume(0,channel)

elif mode == "toggle":
    if currentVolume > 0:
        mixer.setvolume(0,channel)
    else:
        mixer.setvolume(100,channel)
